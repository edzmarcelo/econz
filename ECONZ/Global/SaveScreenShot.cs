﻿using OpenQA.Selenium;
using System;
using System.Text;

namespace ECONZ.Global
{
    public class SaveScreenShot
    {
        public static void SaveScreenshot(IWebDriver driver, string ScreenShotFileName) // Definition
        {
            var folderLocation = ("C:\\Users\\fmarcelo\\Documents\\Visual Studio 2015\\Projects\\ECONZ\\ScreenShots\\");

            if (!System.IO.Directory.Exists(folderLocation))
            {
                System.IO.Directory.CreateDirectory(folderLocation);
            }

            var screenShot = ((ITakesScreenshot)driver).GetScreenshot();
            var fileName = new StringBuilder(folderLocation);

            fileName.Append(ScreenShotFileName);
            fileName.Append(DateTime.Now.ToString("_dd-mm-yyyy_mss"));
            //fileName.Append(DateTime.Now.ToString("dd-mm-yyyym_ss"));
            fileName.Append(".png");
            screenShot.SaveAsFile(fileName.ToString(), System.Drawing.Imaging.ImageFormat.Png);
        }
    }
}
