﻿using OpenQA.Selenium;
using System;

namespace ECONZ.Global
{
    public class InputControl
    {
        //Common driver definition
        public static IWebDriver driver { set; get; }

        public static void TextBox(IWebDriver driver, String Identifier, String IdentifierValue, String Inputvalue)
        {
            if (Identifier == "Id")
            {
                driver.FindElement(By.Id(IdentifierValue)).SendKeys(Inputvalue);
            }

            if (Identifier == "XPath")
            {
                driver.FindElement(By.XPath(IdentifierValue)).SendKeys(Inputvalue);
            }
        }

        public static void ActionBtn(IWebDriver driver, String Identifier, String IdentifierValue)
        {
            if (Identifier == "XPath")
            {
                driver.FindElement(By.XPath(IdentifierValue)).Click();
            }
        }
    }
}
