﻿using System;
using OpenQA.Selenium;
using ECONZ.Global;
using OpenQA.Selenium.Support.PageObjects;

namespace ECONZ.Pages
{
    internal class LoginClass
    {
        public LoginClass()
        {
            PageFactory.InitElements(InputControl.driver, this);
        }

        //Username initialization
        [FindsBy(How = How.Id,Using = "UserName")]
        public IWebElement Username { set; get; }

        //Username initialization
        [FindsBy(How = How.Id, Using = "Password")]
        public IWebElement Password { set; get; }

        //Username initialization
        [FindsBy(How = How.XPath, Using = "html/body/div[3]/form/div/div/div/div[2]/div[3]/input")]
        public IWebElement Loginbutton { set; get; }


        public void LoginSuccessful(IWebDriver driver)
        {
            // Populate Data from Excel
            ExcelLib.PopulateInCollection(@"C:\Users\fmarcelo\Documents\Visual Studio 2015\Projects\ECONZ\Excel\InputData.xlsx", "LoginClass");

            //navigating to the page
            driver.Navigate().GoToUrl(ExcelLib.ReadData(2, "Url"));

            // Identifying the username field
            //IWebElement userName = driver.FindElement(By.XPath(".//*[@id='UserName']"));
            //// Sending the value to the username Textbox
            //userName.SendKeys("aadhi1");

            //InputControl.TextBox(driver, "XPath", ".//*[@id='UserName']", ExcelLib.ReadData(2,"Username"));
            Username.SendKeys(ExcelLib.ReadData(2, "Username"));

            // Identify the password field
            //IWebElement passWord = driver.FindElement(By.Id("Password"));
            //// Sending the value to the password TB
            //passWord.SendKeys("aadhith");
            // combing the above two steps into one step for easiness!
            //driver.FindElement(By.Id("Password")).SendKeys("aadhith");

            //InputControl.TextBox(driver, "Id", "Password", ExcelLib.ReadData(2,"Password"));
            Password.SendKeys(ExcelLib.ReadData(2, "Password"));
            // click on the login button 
            //IWebElement loginButton = driver.FindElement(By.XPath("html/body/div[3]/form/div/div/div/div[2]/div[3]/input"));
            //loginButton.Click();

            //InputControl.ActionBtn(driver, "XPath", "html/body/div[3]/form/div/div/div/div[2]/div[3]/input");
            Loginbutton.Click();

            Console.WriteLine("Login Successfull");
            SaveScreenShot.SaveScreenshot(driver, "Login");

        }
    }
}