﻿using System;
using OpenQA.Selenium;
using System.Threading;
using ECONZ.Global;

namespace ECONZ.Pages
{
    internal class ButtonClass
    {
        internal void addnewRecord(IWebDriver driver)
        {
            // Click on Home tab
            //driver.FindElement(By.XPath(".//*[@id='menu-items']/ul/li[2]/a")).Click()

            InputControl.ActionBtn(driver, "XPath", ".//*[@id='menu-items']/ul/li[2]/a");

            // Click on user account
            //driver.FindElement(By.XPath(".//*[@id='menu-items']/ul/li[2]/ul/li[2]/a")).Click();

            InputControl.ActionBtn(driver, "XPath", ".//*[@id='menu-items']/ul/li[2]/ul/li[2]/a");

            SaveScreenShot.SaveScreenshot(driver, "ButtonPage");

            Thread.Sleep(1000);
            // Click on add new user 
            //driver.FindElement(By.XPath(".//*[@id='grid']/div[1]/a")).Click();
            InputControl.ActionBtn(driver, "XPath", ".//*[@id='grid']/div[1]/a");

            Random r = new Random();
            int n = r.Next();

            String loginTextBoxvalue = "Anything_deleteme17" + n;
            // Enter the User Name
            //driver.FindElement(By.XPath("html/body/div[6]/div[2]/div/div[2]/input")).SendKeys("Anything_deleteme");
            InputControl.TextBox(driver, "XPath", "html/body/div[6]/div[2]/div/div[2]/input", loginTextBoxvalue);

            // for storing the login 
            
            //Enter the Login
            //driver.FindElement(By.XPath("html/body/div[6]/div[2]/div/div[6]/input")).SendKeys(loginTextBoxvalue);
            InputControl.TextBox(driver, "XPath", "html/body/div[6]/div[2]/div/div[6]/input", loginTextBoxvalue);

            //Enter the Email 
            //driver.FindElement(By.XPath("html/body/div[6]/div[2]/div/div[16]/input")).SendKeys("Anything@deletem.com");
            InputControl.TextBox(driver, "XPath", "html/body/div[6]/div[2]/div/div[16]/input", "edz.sabile@gmail.com");

            //Click on Create Button
            //driver.FindElement(By.XPath("html/body/div[6]/div[2]/div/div[21]/a[1]")).Click();
            InputControl.ActionBtn(driver, "XPath", "html/body/div[6]/div[2]/div/div[21]/a[1]");

            try
            {
                // dismiss the dialogue box
                driver.SwitchTo().Alert().Dismiss();
            }
            catch (Exception e) { }
/*
            Thread.Sleep(1500);
            // Verification
            // Clicking on the filter button (Login)
            //driver.FindElement(By.XPath(".//*[@id='grid']/div[3]/div/table/thead/tr/th[3]/a[1]/span")).Click();
            InputControl.ActionBtn(driver, "XPath", ".//*[@id='grid']/div[3]/div/table/thead/tr/th[3]/a[1]/span");

            Thread.Sleep(1500);
            // Enter the username for verification
            //driver.FindElement(By.XPath("html/body/div[5]/form/div[1]/input[1]")).SendKeys(loginTextBoxvalue);
            InputControl.TextBox(driver, "XPath", "html/body/div[5]/form/div[1]/input[1]", "loginTextBoxvalue");

            //Click on filter button
            //driver.FindElement(By.XPath("html/body/div[5]/form/div[1]/div[2]/button[1]")).Click();
            InputControl.ActionBtn(driver, "XPath", "html/body/div[5]/form/div[1]/div[2]/button[1]");

            Thread.Sleep(1500);
            String value = driver.FindElement(By.XPath(".//*[@id='grid']/div[4]/table/tbody/tr/td[1]")).Text;
            

            // verifying
            if (value == "Anything_deleteme")
            {
                Console.WriteLine("Verified");
            }
            else
            {
                Console.WriteLine(value);
                Console.WriteLine("Verification Failes");
                Console.WriteLine(loginTextBoxvalue);

            }
            */
        }

   
    }
}