﻿using ECONZ.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;

namespace ECONZ.Test
{
    class Program
    {
        static void Main(string[] args)
        {
          
        }
    }

    [TestFixture]
    public class Testframework
    {
        // inintializing the browser
        IWebDriver driver = new ChromeDriver();
        [SetUp]
        public void LoginClass()
        {
            // Chrome driver definition
            Global.InputControl.driver = new ChromeDriver();
            
            //Creating an object  for the login class
            LoginClass loginobj = new LoginClass();
            // accessing the loginsuccessfull method using the object
            loginobj.LoginSuccessful(Global.InputControl.driver);
        }
        //[TearDown]
        public void tearDown()
        {
            //closing the driver
            Global.InputControl.driver.Close();
          Console.ReadKey();
        }
        [Test]
        //[ExpectedException(typeof(NoAlertPresentException))]
        public void ButtonClass_AddNeRecord()
        {
            //Creating an object  for the button class
             ButtonClass btnobj = new ButtonClass();
             //accessing the addnewrecord method using the object
            btnobj.addnewRecord(Global.InputControl.driver);
        }
    }
}
