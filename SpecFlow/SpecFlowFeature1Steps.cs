﻿using ECONZ.Global;
using ECONZ.Pages;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace ECONZ
{
    [Binding]
    public class SpecFlowFeature1Steps
    {
        [Given(@"I have logged in to the ECONZ portal")]
        public void GivenIHaveLoggedInToTheECONZPortal()
        {
            //Chrome driver definition
            InputControl.driver = new ChromeDriver();

            //Creating an object for the login class
            LoginClass loginobj = new LoginClass();
            // accessing the loginsuccessful method using the object
            loginobj.LoginSuccessful(InputControl.driver);
        }

        [Given(@"User is able to create a new Button")]
        public void GivenUserIsAbleToCreateANewButton()
        {
            // Creating an object  for the button class
            ButtonClass btnobj = new ButtonClass();
            // accessing the addnewrecord method using the object
            btnobj.addnewRecord(InputControl.driver);
        }
    }
}
